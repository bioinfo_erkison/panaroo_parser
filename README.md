# Panaroo Parser
This repo contains python scripts to parse the output of the pangenome analysis tool, [Panaroo](https://github.com/gtonkinhill/panaroo). 


## Instructions

### Dependencies
>python3.6
[pandas](https://pandas.pydata.org/getting_started.html)
[biopython](https://biopython.org/)
[requests](https://pypi.org/project/requests/)

### Installation
Clone the repository 
```
git clone https://gitlab.com/bioinfo_erkison/panaroo_parser.git
cd panaroo_parser
```

### Quick start

Typically the primary script contains two commands:
- `find_core_and_accessory_genes`: This command extracts the core and accessory genes based on set cut-offs into a csv output. Basic execution:
```
python3 parse_panaroo.py find_core_and_accessory_genes -f <path_to_gene_presence_absence_panaroo_output> -o <path_to_output_dir>
```
- `find_clade_specific_genes`: This command extracts the genes that are specific to a given set of samples. Sample IDs should be provided in a line-delimited text file and must match IDs in gene_presence_absence file. Basic execution:
```
python3 parse_panaroo.py find_clade_specific_genes -f <path_to_gene_presence_absence_panaroo_output> -o <output_prefix> -l <path_to_samples_ids_list_file> -d <path_to_gene_data.csv_panaroo_output_file> -g <genus>
```

