#!/usr/bin/python3

from Bio import SeqIO
import argparse, os

def is_valid_file(parser, arg):
    if not os.path.isfile(arg):
        parser.error('The file {} does not exist!'.format(arg))
    else:
        # File exists so return the filename
        return arg

def parse_arguments():
    description = """
    A package to split a large multifasta file into smaller multifasta files/chunks.
    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        '-f', '--fasta_file',
        type=lambda x: is_valid_file(parser, x),
        help='path to fasta file',
        required=True
    )
    parser.add_argument(
        '-o', '--output_prefix',
        help='output prefix',
        default="batch"
    )
    parser.add_argument(
        '-b', '--batch_size',
        help='number of sequences per output batch', 
        type=int, default=20
    )
    options = parser.parse_args()
    return options

# Code snippet source: https://biopython.org/wiki/Split_large_file
def batch_iterator(iterator, batch_size):
    entry = True  # Make sure we loop once
    while entry:
        batch = []
        while len(batch) < batch_size:
            try:
                entry = next(iterator)
            except StopIteration:
                entry = None
            if entry is None:
                # End of file
                break
            batch.append(entry)
        if batch:
            yield batch

def make_batch_files(fasta_file, output_prefix, batch_size):
    full_fasta = SeqIO.parse(open(fasta_file), "fasta")
    for i, batch in enumerate(batch_iterator(full_fasta, batch_size)):
        filename = "%s_%i.fasta" % (output_prefix, i + 1)
        with open(filename, "w") as f:
            count = SeqIO.write(batch, f, "fasta")
        print("Wrote %i records to %s" % (count, filename))

def main():
    options = parse_arguments()
    make_batch_files(options.fasta_file, options.output_prefix, options.batch_size)

if __name__ == "__main__":
    main()