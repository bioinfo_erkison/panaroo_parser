#!/usr/bin/python3

import os, sys, argparse
from pandas import DataFrame as df
from Bio.Blast import NCBIWWW,NCBIXML

def is_valid_file(parser, arg):
    if not os.path.isfile(arg):
        parser.error('The file {} does not exist!'.format(arg))
    else:
        # File exists so return the filename
        return arg

def parse_arguments():
    description = """
    A package to run blastx to annotate a given set of genes in a multifasta file.
    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    
    subparsers = parser.add_subparsers(
        help='The following commands are available. Type annotate_with_blastx.py <COMMAND> -h for more help on specific commands',
        dest='command'
    )

    subparsers.required = True

    # run_blastx command
    run_blastx = subparsers.add_parser(
        'run_blastx',
        help='Run blastx on given fasta file and create an annotation output. For best results, use gene name as fasta headers.'
    )
    run_blastx.add_argument(
        '-f', '--fasta_file',
        type=lambda x: is_valid_file(parser, x),
        help='path to a fasta file',
        required=True
    )
    # parse_blastx_result command
    parse_blastx_result = subparsers.add_parser(
        'parse_blastx_result',
        help='Parse the .xml output of a blastx search and create an annotation output.'
    )
    parse_blastx_result.add_argument(
        '-x', '--blastx_output_files',
        type=lambda x: is_valid_file(parser, x),
        nargs='+',
        help='paths to at least one blastx output .xml file',
        required=True
    )
    options = parser.parse_args()
    return options
    

def run_blast(fasta_file):
    print("Running blastx ...")
    fasta_seqs = open(fasta_file).read()
    blast_result_handle = NCBIWWW.qblast("blastx", "nr", fasta_seqs, url_base='https://blast.ncbi.nlm.nih.gov/Blast.cgi')
    blast_output_file = "my_blast.xml"
    with open(blast_output_file, "w") as out_handle:
        out_handle.write(blast_result_handle.read())
        print("Saved blast search result as 'my_blast.xml'")
    return blast_output_file

def parse_blast_output(blastx_output_files):
    print("\nParsing blast output...")
    blast_annotation = {}
    for blastx_output_file in blastx_output_files:
        blast_result_handle = open(blastx_output_file)
        blast_records = NCBIXML.parse(blast_result_handle)
        for blast_record in blast_records:
            gene = blast_record.query.split()[0]
            print("\nQuery: %s" % (gene))
            # uncomment next line to sort by best alignment score rather than default e value sorting
            # blast_record.alignments.sort(key = lambda align: max(hsp.score for hsp in align.hsps), reverse=True)
            if blast_record.alignments:
                best_alignment = blast_record.alignments[0]
                annotation_info = {}
                annotation_info["best_blastx_match"] = best_alignment.title
                annotation_info["aln_length"] = best_alignment.length
                annotation_info["accession"] = best_alignment.accession
                for hsp in best_alignment.hsps:
                    annotation_info["e_value"] = hsp.expect
                    annotation_info["ident"] = hsp.identities
                    print(hsp)
                blast_annotation[gene] = annotation_info
            else:
                print("No matches returned for %s" % (gene))
                annotation_info = {**dict.fromkeys([
	                "best_blastx_match",
                    "aln_length",
                    "accession",
                    "e_value",
                    "ident"
                    ],"no match")
                    }
                blast_annotation[gene] = annotation_info
    return blast_annotation

def write_output(blast_annotation):
    if blast_annotation:
        blast_annotation_df = df.from_dict(blast_annotation, orient='index')
        blast_annotation_df.to_csv("gene_annotations.csv", encoding='utf-8')
        print("\n\nSuccess! Annotation results written to output file: 'gene_annotations.csv'\n",'-'*30)
    else:
        print("\nBlast returned no matches for the queries in the multifasta file.",
            "\nWhat you could try:",
            "\nRun blastx with the query file on the NCBI website. If matches are returned, download the blastx results as a '.xml' file.",
            "\nUse the 'parse_blastx_result' command within this script to parse the output.")

def choose_command(options):
    if options.command == 'run_blastx':
        blast_output = run_blast(options.fasta_file)
        blast_annotation = parse_blast_output(blast_output)
        write_output(blast_annotation)
    elif options.command == 'parse_blastx_result':
        blast_annotation = parse_blast_output(options.blastx_output_files)
        write_output(blast_annotation)

def main():
    options = parse_arguments()
    choose_command(options)

if __name__ == "__main__":
    main()


