#!/usr/bin/python3

import argparse
import os
import sys, re
import pandas as pd
import requests, json
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import logging
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

def is_valid_file(parser, arg):
    if not os.path.isfile(arg):
        parser.error('The file {} does not exist!'.format(arg))
    else:
        # File exists so return the filename
        return arg

def parse_arguments():
    description = """
    A package to parse the gene_presence_absence.csv output file of roary/panaroo and extract useful core and accessory genome information
    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    
    subparsers = parser.add_subparsers(
        help='The following commands are available. Type parse_panaroo.py <COMMAND> -h for more help on specific commands',
        dest='command'
    )

    subparsers.required = True
    #parent parser to hold shared arguments
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument(
        '-f', '--presence_absence_filepath',
        type=lambda x: is_valid_file(parser, x),
        help='path to gene_presence_absence.csv file', required=True,
        dest='presence_absence_file'
    )
    parent_parser.add_argument(
        '-o', '--output_prefix',
        help='output filename prefix with no extensions', required=True,
        dest='output'
    )
    parent_parser.add_argument(
        '-c', '--core_min_max', 
        help='comma separated lower and upper percentage occurrence cut-off for genes defined as core. Default: 99,100',
        default='99,100', type=str,
        dest='core'
    )
    parent_parser.add_argument(
        '-sc', '--softcore_min_max', 
        help='comma separated lower and upper percentage occurrence cut-off for genes defined as softcore. Default: 95,99',
        default='95,99', type=str,
        dest='softcore'
    )
    parent_parser.add_argument(
        '-sh', '--shell_min_max', 
        help='comma separated lower and upper percentage occurrence cut-off for genes defined as shell. Default: 15,95',
        default='15,95', type=str,
        dest='shell'
    )
    parent_parser.add_argument(
        '-cl', '--cloud_min_max', 
        help='comma separated lower and upper percentage occurrence cut-off for genes defined as cloud. Default: 0,15',
        default='0,15', type=str,
        dest='cloud'
    )

    # find_clade_specific_genes sub command
    find_clade_specific_genes_command = subparsers.add_parser(
        'find_clade_specific_genes',
        parents=[parent_parser],
        help='Find core and accessory genes associated with specific clades'
    )
    find_clade_specific_genes_command.add_argument(
        '-l', '--samples_ids_list_file',
        type=lambda x: is_valid_file(parser, x),
        help='path to a text file listing IDs of the samples within the clade of interest. IDs must match sample IDs in gene_presence_absence.csv file',
        required=True
    )
    find_clade_specific_genes_command.add_argument(
        '-d', '--gene_data_filepath',
        type=lambda x: is_valid_file(parser, x),
        help='path to gene_data.csv file',
        required=True,
        dest="gene_data_file"
    )
    find_clade_specific_genes_command.add_argument(
        '-g', '--genus_name',
        help='genus name of organism e.g \'Escherichia\'',
        required=True,
        dest='organism'
    )
    find_clade_specific_genes_command.add_argument(
        '-x', '--outside_clade_max', 
        help='maximum allowed percentage occurrence of clade-specific genes in samples outside the clade of interest. Default: 5',
        default=5, type=int,
        dest='max_outside'
    )
    find_clade_specific_genes_command.add_argument(
        '-w', '--within_clade_min', 
        help='minimum allowed percentage occurrence of clade-specific genes in samples within the clade of interest. Default: 95',
        default=95, type=int,
        dest='min_within'
    )

    # find_core_and_accessory_genes sub command
    find_core_and_accessory_genes_command = subparsers.add_parser(
        'find_core_and_accessory_genes',
        parents=[parent_parser],
        help='Find core and accessory genes in all samples'
    )
    
    options = parser.parse_args()
    return options


def prepare_cut_offs(cut_off_options):
    cut_offs = {}
    cut_offs["core_min"],cut_offs["core_max"] = [int(i) for i in cut_off_options.core.split(sep=',')]
    cut_offs["softcore_min"],cut_offs["softcore_max"] = [int(i) for i in cut_off_options.softcore.split(sep=',')]
    cut_offs["shell_min"],cut_offs["shell_max"] = [int(i) for i in cut_off_options.shell.split(sep=',')]
    cut_offs["cloud_min"],cut_offs["cloud_max"] = [int(i) for i in cut_off_options.cloud.split(sep=',')]
    if cut_off_options.command == 'find_clade_specific_genes':
        cut_offs["max_outside"] = int(cut_off_options.max_outside)
        cut_offs["min_within"] = int(cut_off_options.min_within)
    #check continuous intervals
    lst = [cut_offs["cloud_min"], cut_offs["cloud_max"], cut_offs["shell_min"], cut_offs["shell_max"], cut_offs["softcore_min"], cut_offs["softcore_max"], cut_offs["core_min"], cut_offs["core_max"]]
    cut_offs["continuous_cut_off"] = lst[1] == lst[2] and lst[3] == lst[4] and lst[5] == lst[6]
    if cut_offs["continuous_cut_off"] != True:
        print("\nWARNING: Supplied cut off intervals are not continuous! Results may be adversely affected.")
    return cut_offs 

def read_csv_to_dataframe(presence_absence_file):
    location = presence_absence_file
    df = pd.read_csv(location, low_memory=False)
    df.set_index('Gene', inplace=True)
    return df

def categorize_genes(count,num_of_samples,cut_off_options):
    co = prepare_cut_offs(cut_off_options)
    core_lst,softcore_lst,shell_lst,cloud_lst = [],[],[],[]
    for gene, num in count.items():
        percent_present = int(num / num_of_samples * 100)
        if percent_present >= co["core_min"]:
            core_lst.append(gene)
        elif percent_present >= co["softcore_min"] and percent_present < co["softcore_max"]:
            softcore_lst.append(gene)
        elif percent_present >= co["shell_min"] and percent_present < co["shell_max"]:
            shell_lst.append(gene)
        elif percent_present < co["cloud_max"]:
            cloud_lst.append(gene)
    #sort lists
    core_lst.sort()
    softcore_lst.sort()
    shell_lst.sort()
    cloud_lst.sort()
    #write to pandas dataframe and return
    gene_class = {"core" : pd.Series(core_lst), "softcore" : pd.Series(softcore_lst), "shell" : pd.Series(shell_lst), "cloud" : pd.Series(cloud_lst)}
    output_df = pd.DataFrame({key: value for key, value in gene_class.items()})
    return output_df

def determine_clade_specific_genes(count_within_clade,count_outside_clade,num_of_samples_within_clade,num_of_samples_outside_clade,cut_off_options):
    co = prepare_cut_offs(cut_off_options)
    outside_clade_lst, within_clade_lst = [],[]
    #create list of genes occurring in at least 95 percent of samples within the clade of interest
    for gene, num in count_within_clade.items():
        percent_present = int(num / num_of_samples_within_clade * 100)
        if percent_present >= co["min_within"]:
            within_clade_lst.append(gene)
    #create list of genes occurring in over 5 percent of samples outside the clade of interest
    for gene, num in count_outside_clade.items():
        percent_present = int(num / num_of_samples_outside_clade * 100)
        if percent_present >= co["max_outside"]:
            outside_clade_lst.append(gene)
    # remove genes occurring in > 5% of samples outside clade 
    clade_specific_gene_lst = list (set(within_clade_lst) - set(outside_clade_lst))
    #sort list and return
    clade_specific_gene_lst.sort()
    return clade_specific_gene_lst

def request_or_retry():
    log = logging.getLogger('urllib3')
    log.setLevel(logging.WARNING)

    # logging from urllib3 to file
    file_h = logging.FileHandler('panaroo.log')
    file_h.setLevel(logging.WARNING)
    file_log_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_h.setFormatter(file_log_format)
    log.addHandler(file_h)
    retry = Retry(
        total=5,
        status_forcelist=[408, 429, 500, 502, 503, 504],
    )
    adapter = HTTPAdapter(max_retries=retry)
    session = requests.Session()
    session.mount("https://", adapter)
    return session

def get_sequence(annotation_id, gene_data_file):
    with open(gene_data_file, 'r') as f:
        next(f)
        for line in f:
            line = line.split(',')
            #print(line[3])
            if line[3] == annotation_id:
                dna_sequence = line[5]
                break
            else: dna_sequence = ""
    return dna_sequence


def annotate_clade_specific_genes(clade_specific_genes_lst, organism, presence_absence_file, gene_data_file, output_prefix):
    #create output with annotations
    annotation_dct = {}
    sequence_records = []
    for gene in clade_specific_genes_lst:
        with open(presence_absence_file, 'r') as f:
            next(f)
            for line in f:
                line = line.split(',')
                if line[0] == gene:
                    panaroo_annotation = line[2]
                    #get annotation_id to retrieve fasta sequence in gene_data.csv file for unnamed genes
                    try:
                        # exclude first 14 columns and get index of first non-empty cell 
                        i = next(i for i,v in enumerate(line[14:]) if v)
                        annotation_id = line[14:][i]
                    except StopIteration:
                        annotation_id = None
                    break
        #collect seqs for multifasta output
        gene_sequence = get_sequence(annotation_id, gene_data_file)
        if gene_sequence != "":
            sequence_records.append(SeqRecord(
                Seq(gene_sequence),
                id = gene,
                description = annotation_id,
            )
            )
        else: 
            print("Warning: Could not get nucleotide sequence for %s" % (gene))
            pass
            
        #handle incomplete annotations
        if gene.startswith("group_"):
            annotation_dct[gene] = {"annotation":panaroo_annotation, "GO_function": "n/a"}
            continue

        # Use clean gene names to get go annotation
        # replace the multiple tilde (~) characters (if exist) with one e.g. rem~~~rem_2 becomes rem~rem_2
        gene = re.sub('~+', '~', gene)
        # split the gene by the tilde character and use the last one
        gene = gene.split("~")[-1]
        # remove underscore and any number from the end of the gene
        gene = re.sub("_[0-9]","", gene)

        print("Getting GO annotation for %s " %(gene))
        r = request_or_retry().get("https://www.ebi.ac.uk/proteins/api/proteins?offset=0&size=10&exact_gene=%s&organism=%s" %(gene,organism), 
                headers={"Accept" : "application/json"}, 
                timeout=20
                )
        data = json.loads(r.text)
        if len(data) == 0 or r.status_code != 200:
            print("Failed to retrieve GO annotation for %s" %(gene))
            annotation_dct[gene] = {"annotation":panaroo_annotation, "GO_function": "n/a"}
            continue
        go_annot = []
        for entry in data[0]["dbReferences"]:
            if entry["type"] == "GO":
                print(entry)
                term = entry["properties"]["term"]
                go_annot.append(term.split(":")[1])
        go_annot.sort()
        if len(go_annot) == 0: go_annot = ['n/a']
        annotation_dct[gene] = {"annotation":panaroo_annotation, "GO_function": ','.join(go_annot)}
    #write multifasta file
    SeqIO.write(sequence_records, "%s_clade_specific_genes.fasta" % (output_prefix), "fasta")
    print("\nClade specific gene sequences written to \"%s_clade_specific_genes.fasta\"\n" % (output_prefix),'-'*30)
    return annotation_dct

def find_clade_specific_genes(options):
    print("Finding clade specific genes...\n")
    ids_file = options.samples_ids_list_file
    organism = options.organism
    with open(ids_file) as f:
        ids_lst = [line.rstrip() for line in f]
    output = options.output.split(sep='.')[0] #remove extensions if exist
    #read data
    df = read_csv_to_dataframe(options.presence_absence_file)
    #get and parse data in samples within clade
    clade_df = df[df.columns.intersection(ids_lst)].copy()
    clade_df.replace('.+', 1, regex=True, inplace=True)
    clade_df.fillna(0, inplace=True)
    count_clade_df = clade_df.sum(axis=1)
    num_of_samples_within = len(ids_lst)  
    #get and parse data in samples outside clade
    non_clade_df = df.drop([id for id in ids_lst], axis=1).copy()
    non_clade_df.drop(list(df.columns[:13]), axis=1, inplace=True)
    non_clade_df.replace('.+', 1, regex=True, inplace=True)
    non_clade_df.fillna(0, inplace=True)
    count_non_clade_df = non_clade_df.sum(axis=1)
    num_of_samples_outside = count_non_clade_df.max()
    #create output
    clade_specific_genes_lst = determine_clade_specific_genes(count_clade_df, count_non_clade_df, num_of_samples_within, num_of_samples_outside, options)
    if len(clade_specific_genes_lst) == 0:
        print("No clade-specific genes found. You may try adjusting the -x and -w parameters\n", '-'*30)
        clade_specific_genes_df = None
    else:
        clade_specific_genes_df = pd.DataFrame({"clade_specific_accessory_genes": pd.Series(clade_specific_genes_lst)})
        #clade_specific_genes_df.to_csv("%s_clade_specific_accessory_genes.csv" % (output), encoding='utf-8', index=False)
        #all_clade_genes_output_df = categorize_genes(count_clade_df,num_of_samples_within,options)
        #all_clade_genes_output_df.to_csv("%s_all_clade_genes.csv" % (output), encoding='utf-8', index=False)
        #print("\nGene results written to output files: \n%s_clade_specific_accessory_genes.csv \n%s_all_clade_genes.csv\n" % (output, output),'-'*30)
        #annotate
        annotation_dct = annotate_clade_specific_genes(clade_specific_genes_lst, organism, options.presence_absence_file, options.gene_data_file, output)
        clade_specific_gene_annotations_df = pd.DataFrame.from_dict(annotation_dct, orient='index')
        clade_specific_gene_annotations_df.to_csv("%s_clade_specific_accessory_gene_annotations.csv" % (output), encoding='utf-8')
        print("\n\nSuccess! Annotation results written to output file: \n%s_clade_specific_accessory_gene_annotations.csv\n" % (output),'-'*30)
    return clade_specific_genes_df


def find_core_and_accessory_genes(options):
    output = options.output.split(sep='.')[0] #remove extensions if exist
    df = read_csv_to_dataframe(options.presence_absence_file)
    count = df['No. isolates']
    num_of_samples = count.max()
    output_df = categorize_genes(count,num_of_samples,options)
    output_df.to_csv("%s_core_and_accessory_genes.csv" % (output), encoding='utf-8', index=False)
    print("\nSuccess! Results written to output file: \n%s_core_and_accessory_genes.csv" %(output))
    return output_df


def choose_command(options):
    if options.command == 'find_clade_specific_genes':
        find_clade_specific_genes(options)
    elif options.command == 'find_core_and_accessory_genes':
        find_core_and_accessory_genes(options)

def main():
    options = parse_arguments()
    choose_command(options)


if __name__ == "__main__":
    main()


