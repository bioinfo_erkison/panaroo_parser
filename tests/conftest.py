import pytest

@pytest.fixture(scope="module")
def all_options():
    all_options.presence_absence_file = "tests/demo_gene_presence_absence_roary.csv"
    all_options.output = "tests/test_output"
    all_options.core = "99,100"
    all_options.softcore = "95,99"
    all_options.shell = "15,95"
    all_options.cloud = "0,15"
    all_options.samples_ids_list_file = "tests/clade1.txt"
    all_options.max_outside = 5
    all_options.min_within = 95
    all_options.clade_specific_output = "tests/expected_clade1_clade_specific_accessory_genes.csv"
    all_options.total_clade_genes_output = "tests/expected_clade1_all_clade_genes.csv"
    all_options.core_and_accessory_genes_output = "tests/expected_core_and_accessory_genes.csv"
    return all_options

