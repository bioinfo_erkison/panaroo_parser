import pytest
import pandas as pd
from panaroo_parser.parse_panaroo import prepare_cut_offs, find_clade_specific_genes, find_core_and_accessory_genes


def test_always_passes():
    assert True

@pytest.mark.xfail
def test_always_fails():
    assert False

def test_prepare_cut_offs(all_options):
    assert prepare_cut_offs(all_options) == {
        'core_min': 99, 'core_max': 100, 
        'softcore_min': 95, 'softcore_max': 99, 
        'shell_min': 15, 'shell_max': 95, 
        'cloud_min': 0, 'cloud_max': 15, 
        'max_outside': 5, 'min_within': 95, 
        'continuous_cut_off': True
        }

def test_find_clade_specific_genes(all_options):
    #prepare expected output
    expected_output = pd.read_csv(all_options.clade_specific_output)
    expected = expected_output.to_dict()
    #get and prepare output of function
    returned_result = find_clade_specific_genes(all_options)
    result = returned_result.to_dict()
    assert result == expected

def test_find_core_and_accessory_genes(all_options):
    #prepare expected output
    expected_output = pd.read_csv(all_options.core_and_accessory_genes_output)
    expected = expected_output.to_dict()
    #get and prepare output of function
    returned_result = find_core_and_accessory_genes(all_options)
    result = returned_result.to_dict()
    assert result == expected
